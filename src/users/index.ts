export * from "./users_usecases/AssignPhoneToAll";

export interface Usecase<T, R> {
  execute: (t: T) => Promise<R>;
}
