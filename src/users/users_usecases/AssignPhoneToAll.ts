import { Usecase } from "..";
import { User } from "../User";
import { getPhoneNumbers } from "../../utils/fake.generator";

interface AssignToAllRequest {
  users: User[];
}

export class AssignPhoneToAll implements Usecase<AssignToAllRequest, User[]> {
  async execute(cmd: AssignToAllRequest): Promise<User[]> {
    const { users } = cmd;
    let phones = await getPhoneNumbers({ size: cmd.users.length });
    const userUpdated = await Promise.all(
      users.map(async (user: User) => {
        const newPhoneLength = phones.contents.phonenumbers.length;
        if (newPhoneLength === 0) {
          phones = await getPhoneNumbers({ size: users.length });
        }
        return {
          ...user,
          phone_number: phones.contents.phonenumbers.shift()
        };
      })
    );
    return userUpdated;
  }
}
