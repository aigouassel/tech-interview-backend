import { Controller } from ".";
import express from "express";
import { AssignPhoneToAll } from "../users";
const users = require("../users/users.json");

export class UserController implements Controller {
  public router = express.Router();
  public path = "/users";

  constructor() {
    this.initializeRoutes();
  }

  initializeRoutes() {
    this.router.get(this.path, this.assignPhoneNumberToAllUsers);
  }

  async assignPhoneNumberToAllUsers(
    req: express.Request,
    res: express.Response
  ) {
    const result = await new AssignPhoneToAll().execute({ users });
    res.send(result);
  }
}
