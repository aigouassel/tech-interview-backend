import express from "express";
export { UserController } from "./UserController";

export interface Controller {
  path: string;
  router: express.Router;
  initializeRoutes: () => Promise<void> | void;
}
