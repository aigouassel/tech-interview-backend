import { AssignPhoneToAll } from "../users";
import user from "../users/users.json";
import { getPhoneNumbers } from "../utils/fake.generator";
const users = require("../users/users.json");

describe("Test Demo", () => {
  it("should return all users", () => {
    expect(user.length).toBe(88);
  });

  it("should return an array of size 6", async () => {
    const size = 6;
    const phones = await getPhoneNumbers({ size });
    expect(phones.contents.phonenumbers.length).toBe(size);
  });

  it("should not return an array of size 15", async () => {
    const size = 15;
    const phones = await getPhoneNumbers({ size });
    expect(phones.contents.phonenumbers.length).not.toBe(size);
  });

  it("should return users with phone number", async () => {
    const updatedUsers = await new AssignPhoneToAll().execute({ users });
    for (let i = 0; i < updatedUsers.length; i++) {
      expect(!!updatedUsers[i].phone_number).toBe(true);
    }
  });
});
