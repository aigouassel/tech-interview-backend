import bodyParser from "body-parser";
import express from "express";
import { UserController } from "./controllers";
const app = express();

const port = 3000;
app.use(bodyParser.json());
app.use("/", new UserController().router);

app.listen(port, () =>
  console.log(`Server listening at http://localhost:${port}`)
);
